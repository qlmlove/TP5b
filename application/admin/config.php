<?php
//配置文件
return [
    // 视图输出字符串内容替换
    'view_replace_str'       => [
        "__STATIC__"=>"/static",
        "__JS__"=>"/static/js",
        "__CSS__"=>"/static/css",
        "__LAYUI__"=>"/static/layui",
        "__MEDIA__"=>"/media",
    ],
    'jwt'                    => [
        'salt'=>'dfsdfsdJKHK12312JLKJ'
    ]

];
