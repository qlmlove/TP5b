<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\Article as ArticleModel;
use app\common\model\Category as CategoryModel;
use think\Request;

class Article extends AdminBase
{
    /**
     * 显示文章列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $list = ArticleModel::all();
        $this->assign('article_list',$list);
        return view('index');
        //
    }

    /**
     * 显示创建文章页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $category_list = CategoryModel::where('type',1)->select();
        return view('add',['category_list' => $category_list]);
    }

    /**
     * 保存新建的文章
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $res = ArticleModel::create($request->except('publish_time'));
        if ($res) {
            $this->success('文章发布成功',url('index'));
        } else {
            $this->error('遇到未知错误,文章发布失败');
        }
        //
    }


    /**
     * 显示编辑文章表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $category_list = CategoryModel::where('type', 1)->select();
        $article = ArticleModel::get($id);
        return view('edit', ['category_list'=>$category_list,'article'=>$article]);
        //
    }

    /**
     * 保存更新的文章
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $res = ArticleModel::update($request->post(),$id);
        if ($res) {
            $this->success('文章更新成功');
        } else {
            $this->error('文章更新失败');
        }
    }

    /**
     * 删除指定文章
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $res = ArticleModel::destroy($id);
        if ($res) {
            $this->success('文章删除成功');
        } else {
            $this->error('文章删除失败');
        }
        //
    }
}
