<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\AuthGroup as AuthGroupModel;
use think\Request;

class AuthGroup extends AdminBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $auth_group_list = AuthGroupModel::paginate(6);
//        dump($res);
        return view('index', ['auth_group_list' => $auth_group_list]);

    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        //
        $auth_group_list = AuthGroupModel::all();
//        dump($res);
        return view('add', ['auth_group_list' => $auth_group_list]);
    }

    public function auth()
    {
        //
        $auth_group_list = AuthGroupModel::all();
//        dump($res);
        return view('auth', ['auth_group_list' => $auth_group_list]);
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $res = AuthGroupModel::create(input('post.'));
        if ($res) {
            $this->success('添加权限组成功',url('index'));
        } else {
            $this->error('添加权限组失败');
        }
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $auth_group = AuthGroupModel::get($id);
        return view('',['auth_group'=>$auth_group]);
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $res = AuthGroupModel::update($request->post(),$id);
        if ($res) {
            $this->success('修改权限组成功',url('index'));
        } else {
            $this->error('修改权限组失败');
        }
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $res = AuthGroupModel::destroy($id);
        if ($res) {
            $this->success('删除权限组成功',url('index'));
        } else {
            $this->error('删除权限组失败');
        }
        //
    }
}
