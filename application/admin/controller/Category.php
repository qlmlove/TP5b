<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\Category as CategoryModel;
use think\Request;

class Category extends AdminBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $category_list = CategoryModel::all();
        $this->assign('category_list',$category_list);
        return view('index');
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $category_list = CategoryModel::all();
        $this->assign('category_list',$category_list);
        return view('add');
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->post();
//        halt($data);
        if (isset($data['type']) && $data['type']==2){
            $data['pid']=0;
        }
        $res = CategoryModel::create($data);
        if ($res) {
            $this->success('添加分类成功');
        } else {
            $this->error('添加分类失败');
        }
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $current = CategoryModel::get($id);
        $category_list = CategoryModel::all();
        $this->assign('category_list',$category_list);
        return view('edit',['current'=>$current]);
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->post();
//TODO        验证字段合法
        $children = CategoryModel::where(['path' => ['like', "$,{$id},%"]])->column('id');

        if (in_array($data['pid'], $children)) {
            $this->error('不能移动到自己的子分类');
        } else {
            if (CategoryModel::update($data, $id) !== false) {
                $this->success('更新成功');
            } else {
                $this->error('更新失败');
            }
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $res = CategoryModel::destroy($id);
        if ($res) {
            $this->success('删除分类成功', url('index'));
        } else {
            $this->error('删除分类失败');
        }
        //
    }
}
