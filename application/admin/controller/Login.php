<?php

namespace app\admin\controller;

use app\admin\validate\AdminLoginValidate;
use app\common\model\Admin;
use app\common\model\Admin as AdminModel;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use think\Config;
use think\Controller;
use think\Request;
use Lcobucci\JWT\Builder;

class Login extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return view('index');
        //
    }

    public function login(Request $request)
    {
//        只获取三个必须的字段
        $data = $this->request->only(['username', 'password', 'caption']);
//        验证
        (new AdminLoginValidate())->goCheck($data);
        $where['username'] = $data['username'];
//        查询结果
        $res = AdminModel::where($where)->find();
//如果存在该用户,登陆成功,跳转,否则报错
        if ($res&&$res['status']==1) {
            $verify_result = password_verify($data['password'], $res['password']);
            if ($verify_result) {
                $signer = new Sha256();
                $token = (new Builder())
                    ->setId('4f1g23a12aa', true)// Configures the id (jti claim), replicating as a header item
                    ->setIssuedAt(time())// Configures the time that the token was issue (iat claim)
                    ->setNotBefore(time())// 创建时间+60
                    ->setExpiration(time() + 3600)// 过期时间
                    ->set('uid', $res['id'])// 自定义的claim
                    ->set('username', $res['username'])// 自定义的claim
                    ->set('last_login_ip', $res['login_ip'])// 自定义的claim
                    ->set('last_login_time', $res['login_time'])// 自定义的claim
                    ->sign($signer, Config::get('jwt.salt'))//从配置文件读取签名的盐
                    ->getToken(); // 生成Token

//            设置Cookie
                cookie('token', $token, 3600);


                $this->success('登录成功', url('/admin/index'));

            } else {
                $this->error('用户名或者密码错误,请重试');
            }
        } else {
            $this->error('用户名不存在或者账户被禁用');
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
