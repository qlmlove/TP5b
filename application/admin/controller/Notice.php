<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\common\model\Notice as NoticeModel;

class Notice extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $notice_list = NoticeModel::all();
        $this->assign('notice_list', $notice_list);
        return view();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        return view('add');
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->param();
        $res = NoticeModel::create($data);
        if ($res) {
            $this->success('通知发布成功', url('index'));
        } else {
            $this->error('通知发布失败');
        }
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit($id)
    {

        $notice = NoticeModel::get($id);

        $this->assign('notice', $notice);
        return view('edit');
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->param();
        $res = NoticeModel::update($data, $id);
        if ($res) {
            $this->success('更新公告成功', url('index'));
        } else {
            $this->error('更新公告失败');
        }
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
