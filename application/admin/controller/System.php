<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\System as SystemModel;
use think\Request;

class System extends AdminBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return view('site_config');
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

//    更新站点配置
    public function updateSiteConfig(Request $request)
    {
//        获取站点配置,并强制转换为数组
        $site_config = $request->post('site_config/a');
//        将统计代码解码成HTML文本
        $site_config['site_tongji'] = htmlspecialchars_decode($site_config['site_tongji']);
//        将站点配置序列化成字符串,方便存储
        $data['value'] = serialize($site_config);
//        如果网站配置信息存在,则更新,否则就新建
        if (SystemModel::get(['name' => 'site_config'])) {
            $res = SystemModel::update($data, ['name' => 'site_config']);
        } else {
            $res = SystemModel::create(['name' => 'site_config', 'value' => $data['value']]);
        }

        if ($res) {
            $this->success('提交成功');
        } else {
            $this->error('提交失败');
        }
    }

    /**
     * 清除缓存
     */
    public function clear()
    {
        if (delete_dir_file(CACHE_PATH) || delete_dir_file(TEMP_PATH)) {
            $this->success('清除缓存成功');
        } else {
            $this->error('清除缓存失败');
        }
        //
    }
}
