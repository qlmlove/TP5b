<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\Timeline as TimelineModel;
use app\common\model\Category as CategoryModel;
use think\Request;

class Timeline extends AdminBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $timeline_list = TimelineModel::with('category')->select();
        return view('index', ['timeline_list'=>$timeline_list]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        $category_list = CategoryModel::where('type',2)->select();
        return view('add',['category_list' => $category_list]);
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $res = TimelineModel::create($request->post());
        if ($res) {
            $this->success('文章发布成功',url('index'));
        } else {
            $this->error('遇到未知错误,文章发布失败');
        }
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        $category_list = CategoryModel::where('type', 2)->select();
        $timeline = TimelineModel::get($id);
        return view('edit', ['category_list'=>$category_list,'timeline'=>$timeline]);
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $res = TimelineModel::update($request->post(),$id);
        if ($res) {
            $this->success('文章更新成功');
        } else {
            $this->error('文章更新失败');
        }
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $res = TimelineModel::destroy($id);
        if ($res) {
            $this->success('文章删除成功');
        } else {
            $this->error('文章删除失败');
        }
        //
    }
}
