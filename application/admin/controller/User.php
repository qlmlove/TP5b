<?php

namespace app\admin\controller;

use app\common\controller\AdminBase;
use app\common\model\Admin as AdminModel;
use app\common\model\AuthGroup as AuthGroupModel;
use think\Request;

class User extends AdminBase
{
    /**
     * 展示用户登录页面
     *
     * @return \think\Response
     */
    public function index()
    {
        $user_list = AdminModel::all();
//        dump($res);
        return view('index', ['user_list' => $user_list]);
    }

    /**
     * 添加用户页面展示
     *
     * @return \think\Response
     */
    public function add()
    {
        //
        $auth_group_list = AuthGroupModel::all();
        return view('add', ['auth_group_list' => $auth_group_list]);
    }

    /**
     * 保存添加的用户
     *
     * @param  \think\Request $request
     * @return \think\Msg
     */
    public function save(Request $request)
    {
        $data = $request->post();
        if (empty($data['password'])||$data['password']!=$data['confirm_password']) {
            $this->error('两次输入密码不一致,请重试');
        }
        $data['password']=password_hash($data['password'],PASSWORD_BCRYPT);
        unset($data['confirm_password']);
        $res = AdminModel::create($data);
        if ($res) {
            $this->success('添加管理员成功', url('index'));
        } else {
            $this->error('添加管理员失败');
        }
    }

    /**
     * 显示编辑页面
     *
     * @param  int $id
     * @return View
     */

    public function edit($id)
    {
        $user = AdminModel::get($id);
        $auth_group_list = AuthGroupModel::all();
        return view('edit', ['user' => $user, 'auth_group_list' => $auth_group_list]);
        //
    }

    /**
     * 更新用户资料
     *
     * @param  \think\Request $request
     * @param  int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
//        halt($request->post());
        $data = $request->post();
        if ($data['password']!=$data['confirm_password']) {
            $this->error('密码输入不一致');
        }
        unset($data['confirm_password']);
        unset($data['group_id']);
        if (!$data['password']) {
            unset($data['password']);
        } else {
            $data['password']=password_hash($data['password'],PASSWORD_BCRYPT);
        }
        $res = AdminModel::update($data, $id);
        if ($res) {
            $this->success('修改用户成功', url('index'));
        } else {
            $this->error('修改用户失败');
        }
        //
    }

    /**
     * 删除指定用户
     *
     * @param  int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $res = AdminModel::destroy($id);
        if ($res) {
            $this->success('删除用户成功', url('index'));
        } else {
            $this->error('删除用户失败');
        }
        //
    }
}
