<?php
/**
 * Created by PhpStorm.
 * User: lm
 * Date: 18-6-11
 * Time: 下午1:45
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class AdminLoginValidate extends BaseValidate
{
    protected $rule = [
        'username' => 'require',
        'password' => 'require'
    ];

    protected $message = [
        'username.require' => '用户名不能为空',
        'password.require' => '密码不能为空'
    ];
}