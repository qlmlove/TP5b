<?php

namespace app\common\controller;

use app\common\model\Article as ArticleModel;
use app\common\model\Category as CategoryModel;
use app\common\model\Reply as ReplyModel;
use app\common\model\System as SystemModel;
use think\Controller;
use app\common\model\Notice as NoticeModel;

class HomeBase extends Controller
{
    /**
     * 初始化
     */
    public function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub



//        获取网站配置
        $site_config = SystemModel::get(['name' => 'site_config']);
        $site_config = unserialize($site_config['value']);
//        halt($site_config);
        $this->assign('site_config', $site_config);

//        获取通知;
        $site_notice = NoticeModel::all();
        $this->assign('site_notice', $site_notice);


//获取最近文章列表
        $recent_articles = ArticleModel::limit(5)->where('status', 1)->order('update_time', 'desc')->select();
        $this->assign('recent_articles', $recent_articles);


//获取所有文章分类
        $article_categories = CategoryModel::where('type', 1)->select();
        $this->assign('article_categories', $article_categories);


        //获取最近评论
        $recent_replies = ReplyModel::limit(5)
            ->where('status', 1)
            ->order('love', 'desc')
            ->order('create_time', 'desc')
            ->select();
        $this->assign('recent_replies', $recent_replies);

    }
}
