<?php

namespace app\common\model;

use think\Model;

class Notice extends Model
{

    protected $autoWriteTimestamp = true;

    public function setActiveTimeAttr($value)
    {
        return strtotime($value);

    }

    public function getActiveTimeAttr($value)
    {
        return date('Y-m-d H:i:s', $value);

    }
    //
}
