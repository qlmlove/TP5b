<?php

namespace app\common\model;
use traits\model\SoftDelete;
use think\Model;

class Reply extends Model
{
    use SoftDelete; // 使用软删除

    protected $deleteTime = 'delete_time'; // 设置软删除时间的字段名称
    protected $updateTime = false; //

    protected $autoWriteTimestamp = 'timestamp';

    public function article()
    {
        return $this->belongsTo('Article','article_id');
    }


    /**
     * 时间转换获取器
     * @param $value
     * @param $data
     * @return false|string
     */
    public function getTimeTxtAttr($value, $data)
    {
        $before = strtotime($data['create_time']);
        $diff = time() - $before;
        if ($diff < 180) {
            $time = '刚刚';
        } else
            if ($diff < 3600) {
                $time = floor($diff / 60) . '分钟前';
            } else
                if ($diff < 86400) {
                    $time = floor($diff / 3600) . '小时前';
                } else
                    if ($diff < 2592000) {
                        $time = floor($diff / 86400) . '天前';
                    } else {
                        $time = date('Y年m月d日', $before);
                    }

        return $time;
    }
    //
}
