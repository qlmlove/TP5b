<?php
/**
 * Created by PhpStorm.
 * User: lm
 * Date: 18-6-11
 * Time: 下午1:25
 */

namespace app\common\validate;


use think\Exception;
use think\Request;
use think\Validate;
use traits\think\Instance;

/**
 * Base验证器
 * Class BaseValidate
 * @package app\admin\validate
 */
class BaseValidate extends Validate
{

    // 公用验证方法
    public function goCheck($data = ''){
//        实例化请求对象
        $requestObj = Request::instance();
//        如果传递的$data为空,则让它等于请求的参数
        empty($data)&&$data = $requestObj->param();
//        验证,如果通过返回true,否则抛出异常
        if ($this->check($data)){
            return true;
        } else {
//            抛出异常
            return ($this->getError());
            throw new Exception($this->getError());
        }
    }
}