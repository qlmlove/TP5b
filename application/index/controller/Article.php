<?php

namespace app\index\controller;

use app\common\controller\HomeBase;
use app\common\model\Category as CategoryModel;
use think\Controller;
use app\common\model\Article as ArticleModel;
use think\Request;

class Article extends HomeBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return view('index');
        //
    }

    public function read($id)
    {
        $article = articleModel::get($id, 'category');
//        var_dump($article)
        return json(['article' => $article]);
        //
    }


    /**
     * 显示指定的资源
     *
     * @param  int $id
     * @return \think\Response
     */
    public function detail($id)
    {
        $article = new ArticleModel();
        $article = $article->get($id, 'category,reply');
//        $article->setInc('reading');// 增加阅读量
        return view('detail', ['article' => $article]);
    }


    public function timeline($id='')
    {
        $category = new CategoryModel();
        $timeline = $category->get($id, ['timeline'=>function($query){
            $query->order('create_time', 'desc');
        }]);

        return view('timeline', ['timeline'=>$timeline]);
    }

    /**
     * 搜索指定资源
     *
     * @param  string $search
     * @return \think\Response
     */
    public function search(Request $request)
    {

        $article = new ArticleModel();
        $search = $request->param('search');
        $search_list = $article->where('title&content', 'like', "%$search%")->select();

        return view('search', ['search_list' => $search_list]);
    }
}
