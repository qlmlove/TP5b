<?php

namespace app\index\controller;

use app\common\controller\HomeBase;
use app\common\model\Category as CategoryModel;

class Category extends HomeBase
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $categories = CategoryModel::withCount(['Article' => function ($query) {
            $query->order('update_time', 'desc');
        }])->where('type', 1)->select();
        $this->assign('categories', $categories);
        return $this->fetch('a_list');
    }

    /**
     * 时间线
     * @return \think\response\View
     */

    public function timeline()
    {
        $timeline_list = CategoryModel::where('type', '2')->select();
        return view('l_list', ['timeline_list' => $timeline_list]);
    }

    /**
     * 获取单个分类及其下边文章
     * @param $id
     * @return mixed
     */
    public function read($id)
    {
        $category_item = CategoryModel::with(['Article' => function ($query) {
            $query->order('update_time', 'desc');
        }])->where('id', $id)->find();
        $this->assign('category_item', $category_item);
        return $this->fetch('read');
    }
}
