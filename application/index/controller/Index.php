<?php

namespace app\index\controller;

use app\common\controller\HomeBase;
use app\common\model\Article as ArticleModel;

class Index extends HomeBase
{
    public function index()
    {
        $article = new ArticleModel;
//        获取状态为1,并且优先排序置顶的,其次倒序更新时间
        $article_list = $article->with('category')
            ->where('status', 1)
            ->order('is_top', 'desc')
            ->order('update_time', 'desc')
            ->select();
        return view('index', ['article_list' => $article_list]);
    }

    public function about()
    {
        return view('about');
    }
}
