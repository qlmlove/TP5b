<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Validate;
use app\index\validate\ReplyValidate;

class Reply extends Controller
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        return 123;
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $res = new ReplyValidate();
        $res = $res->goCheck();
        if ($res!==true) {
            $this->error($res);
        }
        $data = $request->only('author,email,content,article_id,pid');
        $reply_res = \app\common\model\Reply::create($data);
        if ($reply_res) {
            $this->success('评论成功');
        } else {
            $this->error('评论失败');
        }
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {   $reply = new \app\common\model\Reply();
        $reply = $reply->get($id);
        $reply->setInc('love');// 增加阅读量
        //
        return true;
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
