<?php
/**
 * Created by PhpStorm.
 * User: lm
 * Date: 18-5-31
 * Time: 下午5:17
 */

namespace app\index\validate;


use think\Validate;

class Login extends Validate
{
    protected $rule = [
        'name|姓名' => 'require|min:6|max:8',
        'password|密码' => 'require|min:6|max:8',
        'captcha|验证码' => 'require|captcha'
    ];

    protected $message = [
      'name.require' => '请输入姓名',
      'password.require' => '请输入密码',
      'captcha.require' => '请输入验证码',
      'captcha.captcha' => '验证码错误'
    ];
}