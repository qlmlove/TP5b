<?php
/**
 * Created by PhpStorm.
 * User: lm
 * Date: 18-6-14
 * Time: 上午11:38
 */

namespace app\index\validate;


use app\common\validate\BaseValidate;

class ReplyValidate extends BaseValidate
{
    protected $rule = [
        'author' => 'require',
        'email' => 'require',
        'content' => 'require',
        'captcha' => 'require|captcha'
    ];

    protected $message = [
        'username.require' => '昵称不能为空',
        'email.require' => '邮箱不能为空',
        'content.require' => '评论内容不能为空',
        'captcha.require' => '验证码不能为空',
        'captcha.captcha' => '验证码错误'
    ];

}