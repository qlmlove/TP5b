<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

//return [
//    '__pattern__' => [
//        'name' => '\w+',
//    ],
//    '[hello]'     => [
//        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
//        ':name' => ['index/hello', ['method' => 'post']],
//    ],
//
//];
Route::pattern([
    'search' => '\w+',
    'id' => '\d+'
]);
Route::rule('article/:id', 'index/article/detail');
Route::rule('article/read/:id', 'index/article/read');
Route::rule('articles/:id', 'index/category/read');
Route::rule('articles', 'index/category/index');
Route::rule('search', 'index/article/search');
Route::rule('timeline/:id', 'index/article/timeline');
Route::rule('timeline', 'index/category/timeline');
Route::rule('love/:id', 'index/reply/read', 'GET');
Route::rule('reply', 'index/reply/save', 'POST');
Route::rule('about', 'index/index/about');
