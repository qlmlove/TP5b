<?php

namespace app\test\controller;

use think\Controller;
use think\Request;

class Api extends Controller
{
    /**
     * 使用腾讯翻译API实现翻译功能
     *
     * @return \think\Response
     */
    public function index()
    {
//        腾讯云秘钥
        define('SecretId', 'AKIDtfSHyWi2seXPJGMFE4is24xe9SZT5zgC');
        define('SECRETKEY', 'UoxueCYhdR6Zrytb9XjjBRWP5ZqvnTzJ');
//        公共参数
//        接口方法
        $action = 'TextTranslate';
//        时间戳
        $timestamp = time();
//        随机数
        $nonce = rand(1, 99999);
//        请求域
        $region = 'ap-beijing';#华北地区
//        签名加密方法
        $signatureMethod = "HmacSHA256";
//        项目id
        $projectId = 0;
//        源语言
        $source = 'en';
//        待翻译字符串
        $sourceText = 'hello';
//        目标语言
        $target = 'zh';
//        接口版本
        $version = '2018-03-21';
//TODO 下次将上边参数全部写到下边数组,不用单独分离出来
        $arr = [
            'Action' => $action,
            'SecretId' => SecretId,
            'Timestamp' => $timestamp,
            "Nonce" => $nonce,
            "Region" => $region,
            "SignatureMethod" => $signatureMethod,
            "ProjectId"=>$projectId,
            "Source"=>$source,
            "SourceText"=>$sourceText,
            "Target"=>$target,
            "Version"=>$version

        ];
//        排序
        ksort($arr);
        $str = '';
        foreach ($arr as $k=>$v){
            $str .= $k.'='.$v.'&';
        }
//请求方法
        $method = 'GET';
//        请求主机
        $host = 'tmt.tencentcloudapi.com/?';

//        待加密字符串
        $srcStr = rtrim($method.$host.$str,'&');

//        加密
        $signStr = base64_encode(hash_hmac('sha256', $srcStr, SECRETKEY, true));

//        对签名进行url编码
        $encodeStr = urlencode($signStr);

//        最终请求url
        $srcUrl = 'https://'.$host.$str.'Signature='.$encodeStr;

        return file_get_contents($srcUrl);

    }





    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
